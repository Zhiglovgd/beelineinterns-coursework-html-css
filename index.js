const activeMenuItems = document.querySelectorAll(".menu-item__link_unactive");
for (item of activeMenuItems) {
  item.addEventListener("click", (e) => {
    e.target.classList.toggle("menu-item__link_unactive");
    e.target.classList.toggle("menu-item__link_active");
  });
}
